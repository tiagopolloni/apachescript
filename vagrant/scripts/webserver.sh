#!/bin/bash

# Install httpd
echo "[TASK 1] Installing httpd"
yum install -y httpd >/dev/null 2>&1
systemctl start httpd >/dev/null 2>&1
systemctl enable httpd >/dev/null 2>&1

# Create file
echo "[TASK 2] Creating htmls files"
mkdir -p /var/www/example1.com/html
mkdir -p /var/www/example1.com/log
cat <<'EOF' >> /var/www/example1.com/html/index.html
<!DOCTYPE html>
<html>
    <head>
        <title>teste exaple1</title>
    </head>
    <body>
        <h1>HTTPD TESTE EXAMPLE1</h1>
        <p>Its Working!!!!
        </p>
    </body>
</html>
EOF
mkdir -p /var/www/example2.com/html
mkdir -p /var/www/example2.com/log
cat <<'EOF' >> /var/www/example2.com/html/index.html
<!DOCTYPE html>
<html>
    <head>
        <title>teste example2</title>
    </head>
    <body>
        <h1>HTTPD TESTE EXAMPLE2</h1>
        <p>Its Working!!!!
        </p>
    </body>
</html>
EOF
# Setting Permissions
chown -R $USER:$USER /var/www/example1.com/html
chown -R $USER:$USER /var/www/example2.com/html
chmod -R 775 /var/www
# Configure virtual host
echo "[TASK 3] Configuring Virtual Host"
mkdir -p /etc/httpd/sites-avaliable
mkdir -p /etc/httpd/sites-enabled
sed -i '353s/^/#/' /etc/httpd/conf/httpd.conf
cat <<'EOF' >> /etc/httpd/conf/httpd.conf
IncludeOptional sites-enabled/*.conf  
EOF
cat <<'EOF' >> /etc/httpd/sites-avaliable/example1.com.conf
<VirtualHost *:80>
    ServerName example1.com
    DocumentRoot /var/www/example1.com/html
    CustomLog /var/www/example1.com/log/access.log combined
    ErrorLog /var/www/example1.com/log/error.log
</VirtualHost>
EOF
cat <<'EOF' >> /etc/httpd/sites-avaliable/example2.com.conf
<VirtualHost *:80>
    ServerName example2.com
    DocumentRoot /var/www/example2.com/html
    CustomLog /var/www/example2.com/log/access.log combined
    ErrorLog /var/www/example2.com/log/error.log
</VirtualHost>
EOF

ln -s /etc/httpd/sites-avaliable/example1.com.conf /etc/httpd/sites-enabled/example1.com.conf
ln -s /etc/httpd/sites-avaliable/example2.com.conf /etc/httpd/sites-enabled/example2.com.conf

# Restart httpd
echo "[TASK 4] Restarting httpd"
systemctl restart httpd >/dev/null 2>&1

# Determine IP Address
echo "ip address:" && hostname -I | awk '{ print $3 }'

